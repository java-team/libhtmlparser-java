#!/bin/bash

if [ $# -ne 4 ]
then
    echo "Usage: $0 option version filename"
    echo "If option=--upstream-version, run uupdate after repacking sources."
fi

VERSION=$2
FILENAME=$3

TMPDIR=$(mktemp -d libhtmlparser)
unzip -d $TMPDIR $FILENAME

mv ${TMPDIR}/htmlparser* ${TMPDIR}/libhtmlparser-java-${VERSION}-dfsg0
BASEDIR="${TMPDIR}/libhtmlparser-java-${VERSION}-dfsg0"
SRCDIR="${BASEDIR}/src"
mkdir $SRCDIR
unzip -d $SRCDIR $BASEDIR/src.zip
rm -rf $BASEDIR/src.zip
rm -rf $BASEDIR/lib/*
rm -rf $BASEDIR/docs/javadoc
GZIP=-9 tar -czf libhtmlparser-java_${VERSION}-dfsg0.orig.tar.gz -C ${TMPDIR} libhtmlparser-java-${VERSION}
rm -rf $TMPDIR

if [ $1 = --upstream-version ] ;
then
    uupdate --upstream-version $2 $3
fi
